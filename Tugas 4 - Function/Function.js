//SOAL NOMOR 1
console.log('======NOMOR 1======')
function teriak(){
    return 'Halo Sanbers!'
}
console.log(teriak())

//SOAL NOMOR 2
console.log('\n======NOMOR 2======')
function kalikan(a,b){
    return a*b;
}
var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1,num2)
console.log(hasilKali)

//SOAL NOMOR 3
console.log('\n======NOMOR 3======')

function introduce(nama,umur,alamat,hobi){
    var intro1 = 'Nama saya ' + nama
    var intro2 = ',umur saya ' + umur + ' tahun, '
    var intro3 = 'alamat saya di \n' + alamat
    var intro4 = ', dan saya punya hobby yaitu' + hobi
    return intro1 + intro2 + intro3 + intro4
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 