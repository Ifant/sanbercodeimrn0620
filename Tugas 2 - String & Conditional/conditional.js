//If-else
console.log('Soal if-else:')
var nama = "Jenita"
var peran = "Guard"

if (nama=='' && peran=='' ){
    console.log('Nama harus diisi!')
}else if (nama=='john' && peran==''){
    console.log('Halo John, Pilih peranmu untuk memulai game!')
}else if (nama=='Jane' && peran=='Penyihir'){
    console.log('Selamat datang di Dunia Warewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi warewolf!')
}else if (nama=='Jenita' && peran=='Guard'){
    console.log('Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.')
}else if (nama=='Junaedi' && peran=='Warewolf'){
    console.log('Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
}


// Switch Case
console.log('\nSoal Switch Case:')

var hari = 25
var bulan = 5
var tahun = 2000
var namaBulan;

if (hari<=0 || hari>31 || tahun<1900 || tahun>2200){
    console.log("Tanggal hanya bisa diinput dari 1 - 31")
}else{
    switch(bulan){
        case 1: {namaBulan = 'Januari';}
        case 2: {namaBulan = 'Februari'}
        case 3: {namaBulan = 'Maret'}
        case 4: {namaBulan = 'April'}
        case 5: {namaBulan = 'Mei'}
        case 6: {namaBulan = 'Juni'}
        case 7: {namaBulan = 'Juli'}
        case 8: {namaBulan = 'Agustus'}
        case 9: {namaBulan = 'September'}
        case 10: {namaBulan = 'Oktober'}
        case 11: {namaBulan = 'November'}
        case 12: {namaBulan = 'Desember'}
    }
    console.log(hari,namaBulan,tahun)
}
