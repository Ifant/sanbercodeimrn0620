console.log('==========NOMOR 1==========')
console.log('Realease 0:')

class Animal {
    constructor(hewan) {
        this.nama = hewan;
        this.kaki = 4;
        this.buas = false;
    }
    get name() {
        return this.nama
    }
    get legs() {
        return this.kaki
    }
    get cold_blooded() {
        return this.buas
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\nRelease 1:')


class Ape {
    constructor(hewan) {
        this.nama = hewan
    }
    yell(){
        console.log('Auooo')
    }
}

class Frog {
    constructor(hewan){
        this.nama = hewan
    }
    jump(){
        console.log('hop hop')
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


console.log('\n\n==========NOMOR 2==========')

class Clock {
    constructor({template}) {
        this.template = template
    }

    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    };
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(),1000)
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 