// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
let waktu = 10000
function jalankan(angka){
    if (angka>=books.length){
        return 0
    }else{
        readBooks(waktu,books[angka],function(check){
            waktu = check;
            jalankan(angka+1)
        })
    }
}
jalankan(0)