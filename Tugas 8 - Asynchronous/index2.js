var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let waktu = 10000
function jalankan(angka){
    if (angka==books.length)return 0
    else{
        readBooksPromise(waktu,books[angka])
        .then (function(fulfiled){
            waktu = fulfiled
            jalankan(angka+1)
        })
    }
}
jalankan(0)