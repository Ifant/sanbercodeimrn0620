//NO 1
console.log('=========NOMOR 1=========')
console.log('Looping Pertama:')
var a = 2
while (a<=20){
    console.log(a + '- I love coding');
    a+=2;
}
console.log('\nLooping Kedua:')
while (a>2){
    a-=2;
    console.log(a + '- I will become a mobile developer');
}
//Soal Nomor 2
console.log('\n=========NOMOR 2=========')
for (var b = 1;b<=20;b++){
    if (b % 3 == 0 && b % 2 == 1){
        console.log(b + '- I Love Coding');
    }else if (b % 2 == 0){
        console.log(b + '- Santai');
    }else if (b % 2 == 1){
        console.log(b + '- Berkualitas');
    }
}
//Soal Nomor 3
console.log('\n==========NOMOR 3==========')
var c = 1

while (c<=4){
    var pagar = '#';
    for (var d = 1;d<=8;d++){
        if (d==8){
            console.log(pagar);
        }
        pagar +='#'
    }
    c++;
}

//Soal Nomor 4
console.log('\n==========NOMOR 4==========')
var f = 1

while (f<=7){
    pagar = '#'
    for (var g = 1;g<=f;g++){
        if (g==f){
            console.log(pagar)
            break
        }
        pagar +='#'
    }f++;   
}

//Soal Nomor 5
console.log('\n==========NOMOR 5==========')
var h = 1;
while (h<=8){
    var pagar=''
    for (var i=1;i<=4;i++){
        if (h%2==1){
            pagar += ' #'
        }else{
            pagar +='# '
        }if (i==4){
            console.log(pagar)
        }
    }h++;
}