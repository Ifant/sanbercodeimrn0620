/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
console.log('=====NOMOR 1======')
class Score{
    constructor(subject,points,email){
        this.nama = subject;
        this.nilai = [points[0],points[1],points[2]];
        this.email = email;
    }
    rerata(){
        let n = 0;
        for (let i = 0; i < this.nilai.length;i++){
            n += this.nilai[i]
        }
        let hasil = n / this.nilai.length
        return `${this.nama} mendapatkan rata - rata ${hasil}`
    }
  }
myScore = new Score('ifana',[88,89,99],'ifana.andri@gmail.com');
console.log(myScore.rerata())

  
  /*=========================================== 
    2. SOAL Create Score (10 Poin + 5 Poin ES6)
    ===========================================
    Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
    Function viewScores mengolah data email dan nilai skor pada parameter array 
    lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
    Contoh: 
  
    Input
     
    data : 
    [
      ["email", "quiz-1", "quiz-2", "quiz-3"],
      ["abduh@mail.com", 78, 89, 90],
      ["khairun@mail.com", 95, 85, 88]
    ]
    subject: "quiz-1"
  
    Output 
    [
      {email: "abduh@mail.com", subject: "quiz-1", points: 78},
      {email: "khairun@mail.com", subject: "quiz-1", points: 95},
    ]
  */
  console.log('======NOMOR 2======')
  const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  let i = 1;
  function viewScores(data, subject) {
    let n = data[i][1] + data[i][2] + data[i][3]
    let hasil = n/3
    var obj = {
        email: data[i][0],
        subject,
        points: hasil
    }
    i++;
    console.log(obj)
  }
  
  // TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")
  
  /*==========================================
    3. SOAL Recap Score (15 Poin + 5 Poin ES6)
    ==========================================
      Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
      Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
      predikat kelulusan ditentukan dari aturan berikut:
      nilai > 70 "participant"
      nilai > 80 "graduate"
      nilai > 90 "honour"
  
      output:
      1. Email: abduh@mail.com
      Rata-rata: 85.7
      Predikat: graduate
  
      2. Email: khairun@mail.com
      Rata-rata: 89.3
      Predikat: graduate
  
      3. Email: bondra@mail.com
      Rata-rata: 74.3
      Predikat: participant
  
      4. Email: regi@mail.com
      Rata-rata: 91
      Predikat: honour
  
  */
 console.log('======NOMOR 3======')
  i = 1
  function recapScores(data) {
    let n = data[i][1] + data[i][2] + data[i][3]
    let hasil = n/3
    var obj = {
        email: data[i][0],
        points: hasil,
    }
    let predikat;
    if (obj.points>90){
        predikat = 'honour'
    }else if (obj.points>80){
        predikat = 'graduate'
    }else if (obj.points>70){
        predikat = 'participant'
    }else{
        console.log('Invalid Input!')
    }
    console.log(`email: ${obj.email}`)
    console.log(`Rata-rata: ${obj.points}`)
    console.log(`Predikat: ${predikat}\n`)
    i++;
    
  }

  recapScores(data)
  recapScores(data)
  recapScores(data)
  