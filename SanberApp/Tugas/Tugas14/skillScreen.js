import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList, TouchableOpacity, StyleSheet,Image, SafeAreaViewBase } from 'react-native';
import datas from './skillData.json'

import { MaterialCommunityIcons,Ionicons,FontAwesome5 } from '@expo/vector-icons';

// const Skill = (props) => {
//     return(
//         <View style={ styles.kotakSkill }>
//             <FontAwesome5 name="react" size={100} color="black" />
//             <View>
//                 <Text style={{fontSize:30,fontWeight: "bold"}}>props.title</Text>  
//                 <Text style={{fontSize:19}}>props.jenis</Text>
//                 <Text style={{fontSize:40}}>props.percentage</Text> 
//             </View>
//             <Ionicons name="ios-arrow-forward" size={100} color="black" />
            
//         </View>
//     )
// } 


export default class skillScreen extends Component{

    renderItem = ({ item }) => {
        return (
            <View style={ styles.kotakSkill }>
                <MaterialCommunityIcons name={item.logoUrl} size={60} color="black" />
                <View>
                    <Text style={{fontSize:30,fontWeight: "bold"}}>{item.skillName}</Text>  
                    <Text style={{fontSize:19}}>{item.categoryName}</Text>
                    <Text style={{fontSize:40}}>{item.percentageProgress}</Text> 
                </View>
                <Ionicons name="ios-arrow-forward" size={100} color="black" />
            </View>
        )
      }
      
    render(){
        return(
            <View >
                <View style={styles.header}>
                    <Image source={require('../Tugas13/Image/SkillyLogo.png')} style={styles.logo}></Image>
                </View>
                <ScrollView>
                    <View style={{flexDirection:"row", justifyContent: "center",padding:50}}>
                        <View style={styles.bgProfile}>
                            <Ionicons name="md-person" size={50} color="white" />
                        </View>
                        <View style={{justifyContent: "center"}}>
                            <Text style={styles.textProfile}>Ifana Andriansyah</Text>
                            <Text style={styles.textProfile} style = {{fontSize:16}}>React Native Developer</Text>
                        </View>
                    </View>
                    <Text style={styles.textSkill}>Your Skill</Text>
                    <View style={ styles.garis}/>
                    <View style = {{alignItems:"center"}}>
                        <FlatList
                            data={datas.items}
                            renderItem={this.renderItem}
                            keyExtractor={(item)=>item.id}
                    />
                    </View>
                </ScrollView>
                
            </View>
        )
    }
}    


const styles = StyleSheet.create({
    header: {
        backgroundColor: '#4C97EE',
        height:80,
        alignItems: "center",
        justifyContent: "center"
    },
    logo:{
        width:180-85,
        height: 100-55,
    },
    bgProfile:{
        width:80,
        height:80,
        backgroundColor: '#4C97EE',
        borderRadius: 80/2,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 8,
      },
    textProfile:{
        color: '#000000',
        opacity: 0.79,
        fontSize: 24
    },
    textSkill:{
        color: '#000000',
        opacity: 0.79,
        fontSize: 30,
        textAlign: "center"
    },
    garis:{
        backgroundColor: '#4C97EE',
        height: 4,
        width : 350,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 10,
        borderRadius: 2
    },
    kotakSkill: {
        width: 350,
        height: 150,
        borderRadius: 25,
        marginBottom: 10,
        opacity: 0.60,
        backgroundColor: '#4C97EE',
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around"
    }
})