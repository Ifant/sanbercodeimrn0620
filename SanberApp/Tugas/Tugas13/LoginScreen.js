import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';

export default class Login extends Component{
    render(){
        return (
            <View style = {styles.bg}>
                <Image source={require('./Image/SkillyLogo.png')} style = {styles.logoSkilly}/>
                <View style = {{flexDirection: "column", alignItems:  "center",justifyContent:"center" }}> 
                    <View style = {styles.kolom}>
                        <Text style = {styles.textColomn}>Username </Text>
                    </View>
                    <View style = {styles.kolom}>
                        <Text style = {styles.textColomn}>Password </Text>
                    </View>
                </View>
                <View style = {{marginTop: 35, alignItems: "center"}}>
                    <View style = {styles.kolomBawah}>
                        <Text style = {styles.textBawah}>Login</Text>
                    </View>
                    <View style = {styles.kolomBawah}>
                        <Text style = {styles.textBawah}>Register</Text>
                    </View> 
                </View>

                
                
            </View> 
        )
    }
}


const styles = StyleSheet.create({
    bg:{
        flex: 1,
        backgroundColor: '#4C97EE'
    },
    logoSkilly: {
        width:180,
        height: 100,
        margin: 100,
        marginBottom: 75,
    },
    kolom:{
        backgroundColor: '#FFFFFF',
        margin : 3,
        width: 235,
        height: 55,
        borderRadius: 5,
        justifyContent: "center",
    },
    textColomn: {
        color: '#000000',
        fontSize: 19,
        marginLeft: 19,
        opacity: 0.35
    },
    kolomBawah:{
        width: 235,
        height: 34,
        margin: 3,
        backgroundColor: '#DED3D3',
        borderRadius: 9,
        justifyContent: "center",
        alignItems: "center"
    },
    textBawah:{
        color: '#000000',
        fontSize: 16,
        fontWeight: "bold",
        opacity: 0.63
    }
})


