import React, { Component } from 'react';

import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

import { AntDesign } from '@expo/vector-icons';

export default class About extends Component{
    render(){
        return (
            <View >
                <ScrollView>
                    <View style = {{alignItems:"stretch"}}>
                        <View style = {styles.atas}> 
                            <Image source = {require('./Image/SkillyLogo.png')} style = {styles.logo}></Image>
                        </View>
                    </View>
                    <Text style = {styles.tentang}>Tentang Saya</Text>
                    <View style = {{alignItems : "center"}}>
                        <View style = {styles.eclipse}>
                            <View style = {styles.vectorKepala}/>
                            <View style = {styles.vectorBadan}/>
                        </View>
                    </View>
                    <Text style = {styles.nama}> Ifana Andriansyah </Text>
                    <Text style = {styles.ket}> React Native Developer</Text>
                    <Text style = {styles.textBawah}>Portofolio:</Text>
                    <View style = {{flexDirection: "row", justifyContent: "center"}}>
                        <View style = {styles.bgGit}>
                         <AntDesign name="gitlab" size={40} color="black" />
                        </View>
                        <View style = {styles.bgGit}>
                         <AntDesign name="github" size={40} color="black" />
                        </View>
                    </View>
                    <Text style = {styles.textBawah}>Find Me on Social Media:</Text>
                    <View style = {{flexDirection: "row", justifyContent: "center"}}>
                        <AntDesign name="instagram" size={24} color="#4C97EE" style = {styles.sosmed}/>
                        <AntDesign name="facebook-square" size={24} color="#4C97EE" style = {styles.sosmed}/>
                        <AntDesign name="linkedin-square" size={24} color="#4C97EE" style = {styles.sosmed}/>
                        
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    atas:{
        height : 60,
        backgroundColor: '#4C97EE',
        alignItems: "center",
        justifyContent: "center"
    },
    logo:{
        width:180-85,
        height: 100-55,
    },
    tentang: {
        fontWeight: "bold",
        fontSize: 36,
        opacity: 0.74,
        marginTop: 54,
        marginBottom: 15,
        textAlign: "center"
    },
    eclipse: {
        width: 200,
        height: 200,
        borderRadius: 200/2,
        backgroundColor: '#EFEFEF',
        alignItems: "center"
    },
    vectorKepala:{
        width: 80,
        height: 80,
        marginTop: 14,
        borderRadius: 80/2,
        backgroundColor: '#CACACA'
    },
    vectorBadan:{
        width : 130,
        height: 70,
        marginTop: 1,
        borderTopEndRadius: 120/2,
        borderTopStartRadius: 120/2,
        backgroundColor: '#CACACA'
    },
    nama: {
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 15,
        opacity: 0.74
    },
    ket:{
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center",
        opacity: 0.74,
        color: '#4C97EE'
    },
    textBawah: {
        fontSize: 15,
        textAlign: "center",
        marginTop: 20, 
        marginBottom:10,
        fontStyle: 'italic'
    },
    bgGit:{
        width: 70,
        height: 70,
        borderRadius: 70/2,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#4C97EE',
        alignItems: "center",
        justifyContent: "center"
    },
    gitHub:{
        height: 39,
        width: 45
    },
    gitLab: {
        width: 95,
        height: 35
    },
    sosmed:{
        marginLeft: 9,
        marginRight: 9
    }
    
})