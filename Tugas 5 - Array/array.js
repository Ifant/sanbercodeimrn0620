console.log("=====SOAL NOMOR 1=====") 
 
 function range(start,finish){
    var array = [];
    var k = 0;
     if (start <= finish){
        for (var i = start; i <= finish;i++){
            array[k] = i;
            k++;
        }
     }else if (finish < start ){
         for (var i=start;start>=finish;i++){
                array[k] = start;
                start--;
                k++;
            }
     }else{
         array[k]= -1
     }
     console.log(array)
     
 }
 
 console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 console.log(range(1)) // -1
 console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
 console.log(range(54, 50)) // [54, 53, 52, 51, 50]
 console.log(range()) // -1 
 


 console.log('\n=====SOAL NOMOR 2=====')
 function rangeWithStep(start,finish,interval){
     array = []
     k = 0;
     if (start <= finish){
         for (var i = start;i<=finish;i++){
            array[k] = i
            i = i + interval - 1;
            k++;
         }
     }else if (start >finish){
         for(var i = start;start>=finish;i++){
             array[k] = start
             start-=interval;
             k++
         }
     }
     console.log(array)
 }
 
 console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
 console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
 console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
 console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

 console.log('=====SOAL NOMOR 3=====')
 function sum(start,finish,interval){
     var jumlah=0;
     array = []
      k = 0;
      if (start <= finish){
          for (var i = start;i<=finish;i++){
             array[k] = i
             jumlah+=array[k]
             i+=interval - 1;
             k++;
          }
      }else if (start >finish){
          for(var i = start;start>=finish;i++){
              array[k] = start
              jumlah+=array[k]
              start-=interval - 1;
              k++
          }
      }else {
          array[0] = 0
          jumlah+=array[0]
      }
      console.log(jumlah)
 }
 
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("=====SOAL NOMOR 4======")
function dataHandling(input){
    for (var i=0;i<4;i++){
        for (var j=0;j<5;j++){
            if (j==0){
                console.log('Nomor ID:',input[i][j])
            }
            else if (j==1){
                console.log('Nama Lengkap:',input[i][j])
            }
            else if (j==2){
                console.log('TTL:',input[i][j])
            }
            else if (j==3){
                console.log('Hobip:',input[i][j],'\n')
            }
        }
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input)

console.log('\n=====SOAL NOMOR 5=====')

function balikKata(kalimat){
    var jumlah = kalimat.length
    var balik = ''
    for (i=jumlah;i>=0;i--){
        balik += kalimat[i]
    }
    console.log(balik)
    

}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

